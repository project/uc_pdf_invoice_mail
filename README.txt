CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation


INTRODUCTION
------------

This module generates PDF of invoice and sends to customer in order mail.


REQUIREMENTS
------------

This module requires the following:
 * PHP 7.2 or greater
 * Drupal core 8.0.0 or greater
 * Dompdf 0.8.5 or greater


INSTALLATION
------------

 * Strongly recommend installing this module using composer:
   composer require drupal/uc_pdf_invoice_mail

 * Also you can install DomPDF separately using composer:
   composer require dompdf/dompdf:^0.8.5
